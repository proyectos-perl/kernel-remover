#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Kernel_Remover' ) || print "Bail out!\n";
}

diag( "Testing Kernel_Remover $Kernel_Remover::VERSION, Perl $], $^X" );
