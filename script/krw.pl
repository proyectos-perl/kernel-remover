#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  krw.pl
#
#        USAGE:  ./krw.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Nelo R. Tovar (NT), 
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  12/07/11 10:39:42
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;
use 5.010;
use Kernel_Remover;
use UI::Dialog::Backend::Whiptail;

# Crear una instancia de UI::Dialog
my $dialogo = UI::Dialog::Backend::Whiptail->new( 
    backtitle => 'Kernel Remover', 
    title => 'Kernel Remover', width => 65, height => 15, listheight => 10, 
    debug => 0 );

# Seleccionar el/los kernel(s) a eliminar
my @kernel_to_remove = $dialogo->checklist( text => 'Kernels instalados',
    list => [map(($_, []), list_installed_kernel)])
    or exit(0);

if ($dialogo->state() eq 'OK'){

    if ($>){
        $dialogo->msgbox(text => 'Se debe ejecutar como root');
        exit(0);
    }

    $dialogo->gauge_start(text => 'Limpiando', percentage => scalar @kernel_to_remove);
    foreach my $kernel_id (@kernel_to_remove) {
        $dialogo->gauge_inc(1);
        $dialogo->gauge_text("Kernel: $kernel_id");
        say "Error" unless remove_kernel($kernel_id);
    };
    $dialogo->gauge_stop();
}
